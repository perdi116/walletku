package com.walletku.authentication.feign;

import com.walletku.authentication.dto.RegisterRequest;
import com.walletku.common.dto.MemberDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("member/feign")
public interface MemberClient {
    @PostMapping("register")
    MemberDTO registerMember(@RequestBody RegisterRequest request);
}
