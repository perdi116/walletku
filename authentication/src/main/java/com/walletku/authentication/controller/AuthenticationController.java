package com.walletku.authentication.controller;

import com.google.gson.Gson;
import com.walletku.authentication.dto.LoginRequest;
import com.walletku.authentication.service.AuthenticationService;
import com.walletku.authentication.dto.RegisterRequest;
import com.walletku.common.constant.LoggerType;
import com.walletku.common.dto.MemberDTO;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping
public class AuthenticationController {

    private final AuthenticationService authenticationService;
    private final Gson gson;

    @PostMapping("register")
    public ResponseEntity<MemberDTO> register(@Valid @RequestBody RegisterRequest request) {
        logger(LoggerType.REQUEST, "Register", request);

        MemberDTO response = authenticationService.register(request);

        logger(LoggerType.RESPONSE, "Register", response);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("login")
    public ResponseEntity<HashMap<String, String>> login(@Valid @RequestBody LoginRequest request) {
        logger(LoggerType.REQUEST, "Login", request);

        String token = authenticationService.login(request);

        HashMap<String, String> response = new HashMap<>();
        response.put("token", token);

        logger(LoggerType.RESPONSE, "Login", response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private void logger(LoggerType type, String location, Object data) {
        StringBuilder builder = new StringBuilder();
        if (type.equals(LoggerType.REQUEST)) {
            builder.append("[START] {} Request : {}");
        } else {
            builder.append("[END] {} Response : {}");
        }

        log.info(builder.toString(), location, gson.toJson(data));
    }
}
