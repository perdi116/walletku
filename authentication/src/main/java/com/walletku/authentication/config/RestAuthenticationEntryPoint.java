package com.walletku.authentication.config;

import com.google.gson.Gson;
import com.walletku.common.response.CommonResponse;
import com.walletku.common.response.CommonStatusResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component("restAuthenticationEntryPoint")
@AllArgsConstructor
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final Gson gson;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) throws IOException {
        CommonStatusResponse status = CommonStatusResponse.builder()
                .failure(true)
                .code(HttpStatus.UNAUTHORIZED.value())
                .messages(List.of("Mobile Number / Password Incorrect"))
                .build();

        CommonResponse<Object> output = CommonResponse.builder()
                .status(status)
                .data(null)
                .build();

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getOutputStream().println(gson.toJson(output));
    }
}
