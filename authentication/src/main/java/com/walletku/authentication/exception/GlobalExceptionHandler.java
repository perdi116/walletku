package com.walletku.authentication.exception;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.walletku.common.response.ErrorResponse;
import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.*;

@Slf4j
@RestControllerAdvice
@AllArgsConstructor
public class GlobalExceptionHandler {

    private final Gson gson;

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());

        ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Server Error", details);

        logger(String.valueOf(ex), gson.toJson(error));
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public final ResponseEntity<Object> handleBadCredentialExceptions(BadCredentialsException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());

        ErrorResponse error = new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), "Mobile Number / Password Incorrect", details);

        logger(ex.getMessage(), gson.toJson(error));
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<String> details = new ArrayList<>();

        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();

            details.add(String.join(" ", fieldName, errorMessage));
        });

        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "Bad Request", details);

        logger(ex.getMessage(), gson.toJson(error));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Object> handleAuthenticationExceptions(UsernameNotFoundException ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());

        ErrorResponse error = new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), "Authentication Error", details);

        logger(ex.getMessage(), gson.toJson(error));
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Object> handleAuthenticationExceptions(FeignException ex) {
        JsonObject feignResponse = gson.fromJson(ex.contentUTF8(), JsonObject.class);
        Integer code = feignResponse.get("code").getAsInt();
        String message = feignResponse.get("message").getAsString();
        JsonArray jsonArray = feignResponse.get("details").getAsJsonArray();

        List<String> details = new ArrayList<>();

        if (!jsonArray.isEmpty()) {
            for (JsonElement jsonElement : jsonArray) {
                details.add(jsonElement.getAsString());
            }
        }

        ErrorResponse error = new ErrorResponse(
                code,
                message,
                details
        );

        logger(feignResponse.get("message").getAsString(), gson.toJson(error));
        return new ResponseEntity<>(error, HttpStatusCode.valueOf(ex.status()));
    }

    private void logger(String message, String response) {
        log.error("Throw {}, Response : {}", message, response);
    }
}
