package com.walletku.authentication.service;

import com.walletku.authentication.dto.LoginRequest;
import com.walletku.authentication.feign.MemberClient;
import com.walletku.authentication.dto.RegisterRequest;
import com.walletku.common.dto.MemberDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class AuthenticationService {
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final MemberClient memberClient;

    public MemberDTO register(RegisterRequest request) {
        request.setPassword(passwordEncoder.encode(request.getPassword()));

        return memberClient.registerMember(request);
    }

    public String login(LoginRequest loginRequest) {
        Authentication authenticate = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getMobileNumber(),
                        loginRequest.getPassword()
                )
        );

        if (!authenticate.isAuthenticated()) throw new UsernameNotFoundException("Member Not Found");
        return jwtService.generateToken(loginRequest.getMobileNumber());
    }
}
