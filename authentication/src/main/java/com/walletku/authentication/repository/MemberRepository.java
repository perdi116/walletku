package com.walletku.authentication.repository;

import com.walletku.authentication.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Integer> {
    Optional<Member> findByMobileNumber(String mobileNumber);

}
