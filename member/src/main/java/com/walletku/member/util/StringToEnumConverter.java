package com.walletku.member.util;

import com.walletku.common.constant.Gender;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class StringToEnumConverter implements Function<String, Gender> {

    @Override
    public Gender apply(String source) {
        return Gender.valueOf(source.toUpperCase());
    }
}
