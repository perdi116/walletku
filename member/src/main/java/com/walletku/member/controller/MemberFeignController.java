package com.walletku.member.controller;

import com.google.gson.Gson;
import com.walletku.common.dto.MemberDTO;
import com.walletku.common.exception.BusinessException;
import com.walletku.member.dto.MemberRegisterRequest;
import com.walletku.member.service.MemberService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("feign")
@AllArgsConstructor
public class MemberFeignController {
    private final MemberService memberService;
    private final Gson gson;

    @PostMapping("register")
    public MemberDTO registerMember(@RequestBody MemberRegisterRequest request) throws BusinessException {
        log.info("[START] Register Member Feign Request : {}", gson.toJson(request));

        MemberDTO response = memberService.registerMember(request);

        log.info("[END] Register Member Feign Response : {}", gson.toJson(response));
        return response;
    }
}
