package com.walletku.member.controller;

import com.google.gson.Gson;
import com.walletku.common.response.CommonResponse;
import com.walletku.common.response.CommonStatusResponse;
import com.walletku.member.service.MemberService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping
public class MemberController {

    private final MemberService memberService;
    private final Gson gson;

    @GetMapping
    public ResponseEntity<CommonResponse<Object>> getAllMembers(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String mobileNumber,
            HttpServletRequest httpServletRequest
    ) {
        log.info("[START] Get All Member Request, Param : {}", httpServletRequest.getQueryString());

        CommonResponse<Object> response = CommonResponse.builder()
                .status(new CommonStatusResponse(false, HttpStatus.OK.value(), null))
                .data(memberService.getAllMember(name, email, mobileNumber))
                .build();

        log.info("[END] Get All Member Response : {}", gson.toJson(response));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
