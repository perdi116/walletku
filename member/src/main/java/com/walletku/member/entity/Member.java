package com.walletku.member.entity;

import com.walletku.common.constant.Gender;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Member {
    @Id
    @SequenceGenerator(
            name = "member_seq_generator",
            sequenceName = "member_seq_generator"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "member_seq_generator"
    )
    private Integer id;
    private String name;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String mobileNumber;
    private String password;
    private Date dateOfBirth;
    @Enumerated(EnumType.STRING)
    private Gender gender;
}
