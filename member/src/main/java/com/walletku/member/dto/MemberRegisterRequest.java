package com.walletku.member.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberRegisterRequest {
        private String name;
        private String email;
        private String mobileNumber;
        private String password;
        private Date dateOfBirth;
        private String gender;
}
