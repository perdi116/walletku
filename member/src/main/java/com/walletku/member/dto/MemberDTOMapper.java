package com.walletku.member.dto;

import com.walletku.common.dto.MemberDTO;
import com.walletku.member.entity.Member;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class MemberDTOMapper implements Function<Member, MemberDTO> {
    @Override
    public MemberDTO apply(Member member) {
        return MemberDTO.builder()
                .name(member.getName())
                .email(member.getEmail())
                .mobileNumber(member.getMobileNumber())
                .gender(member.getGender().toString())
                .dateOfBirth(member.getDateOfBirth())
                .build();
    }
}
