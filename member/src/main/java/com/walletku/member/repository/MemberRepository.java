package com.walletku.member.repository;

import com.walletku.member.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Integer> {
    boolean existsByMobileNumber(String mobileNumber);
}
