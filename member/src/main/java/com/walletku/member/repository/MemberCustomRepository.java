package com.walletku.member.repository;

import com.walletku.member.entity.Member;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MemberCustomRepository {

    @PersistenceContext
    EntityManager entityManager;

    public List<Member> findMembersWithFilter(String name, String email, String mobileNumber) {
        StringBuilder query = new StringBuilder("SELECT m FROM Member m WHERE 1=1");

        if (name != null) {
            query.append(" AND m.name LIKE :name");
        }

        if (email != null) {
            query.append(" AND m.email = :email");
        }

        if (email != null) {
            query.append(" AND mobileNumber = :mobileNumber");
        }

        Query getQuery = entityManager.createQuery(query.toString(), Member.class);

        if (name != null) {
            getQuery.setParameter("name", name);
        }

        if (email != null) {
            getQuery.setParameter("email", email);
        }

        if (mobileNumber != null) {
            getQuery.setParameter("mobileNumber", mobileNumber);
        }

        return getQuery.getResultList();
    }
}
