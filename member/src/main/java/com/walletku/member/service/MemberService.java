package com.walletku.member.service;

import com.walletku.common.dto.MemberDTO;
import com.walletku.common.exception.BusinessException;
import com.walletku.member.dto.MemberDTOMapper;
import com.walletku.member.dto.MemberRegisterRequest;
import com.walletku.member.util.StringToEnumConverter;
import com.walletku.member.entity.Member;
import com.walletku.member.repository.MemberCustomRepository;
import com.walletku.member.repository.MemberRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;
    private final MemberCustomRepository memberCustomRepository;
    private final MemberDTOMapper memberDTOMapper;
    private final StringToEnumConverter stringToEnumConverter;

    public MemberDTO registerMember(MemberRegisterRequest request) throws BusinessException {
        if (memberRepository.existsByMobileNumber(request.getMobileNumber()))
            throw new BusinessException(100, "Member exist");

        Member member = Member.builder()
                .name(request.getName())
                .email(request.getEmail())
                .mobileNumber(request.getMobileNumber())
                .password(request.getPassword())
                .dateOfBirth(request.getDateOfBirth())
                .gender(stringToEnumConverter.apply(request.getGender()))
                .build();

        return memberDTOMapper.apply(memberRepository.save(member));
    }

    public List<MemberDTO> getAllMember(String name, String email, String mobileNumber) {
        List<Member> members = memberCustomRepository.findMembersWithFilter(name, email, mobileNumber);

        return members.stream().map(memberDTOMapper).toList();
    }
}
