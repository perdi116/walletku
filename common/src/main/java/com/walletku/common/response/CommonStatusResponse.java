package com.walletku.common.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommonStatusResponse {
    private Boolean failure;
    private Integer code;
    private List<String> messages;
}
