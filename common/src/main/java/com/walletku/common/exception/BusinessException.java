package com.walletku.common.exception;

import lombok.*;

@Setter
@Getter
public class BusinessException extends Exception{
    private final Integer code;
    private final String message;

    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
