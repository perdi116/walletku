package com.walletku.common.constant;

public enum LoggerType {
    REQUEST,
    RESPONSE
}
