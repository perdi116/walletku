package com.walletku.common.constant;

import com.google.gson.annotations.SerializedName;

public enum Gender {
    @SerializedName("0")
    MALE,
    @SerializedName("1")
    FEMALE
}
